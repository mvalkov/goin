package main

import (
	"flag"
	"os"
	"path/filepath"
	"strings"

	"github.com/meehalkoff/loggi"
)

func main() {

	run := flag.String("run", "install", "-run update or -run install")
	goroot := flag.String(
		"goroot",
		filepath.Join(os.Getenv("HOME"), "Go/SDK"),
		"-goroot /path/to/gosdk",
	)
	gopath := flag.String(
		"gopath",
		filepath.Join(os.Getenv("HOME"), "Go/WorkPlace"),
		"-gopath /path/to/workplace",
	)
	flag.Parse()

	goin := Goin{
		Downloader: Downloader{
			RootURL: "https://golang.org/dl/",
			TmpFile: "/tmp/goin.tar.gz",
		},
	}

	switch *run {
	case "install":

		loggi.Info("Установка.")

		goin.GOPATH = *gopath
		goin.GOROOT = *goroot

		// Предустановочные проверки
		if err := goin.PreInstall(); err != nil {
			loggi.Fatal(err)
		}

		// Поиск последней стабильной версии Го, подходящего для этой системы
		if err := goin.FindURL(); err != nil {
			loggi.Fatal(err)
		}

		// Загрузка последней стабильной версии Го, подходящего для этой системы
		if err := goin.Download(); err != nil {
			loggi.Fatal(err)
		}

		// Распаковка загруженного архива c Го в GOROOT
		if err := goin.Untar(goin.TmpFile, goin.GOROOT); err != nil {
			loggi.Fatal(err)
		}

		// Сохраняет переменные окружения
		if err := goin.WriteGoEnv(); err != nil {
			loggi.Fatal(err)
		}

		// Чистит за собой мусор
		if err := goin.Clean(); err != nil {
			loggi.Fatal(err)
		}

		loggi.Info("Установлено. Перелогинься.")

	case "update":

		// Проверка перед обновлением
		if err := goin.PreUpdate(); err != nil {
			loggi.Fatal(err)
		}

		// Поиск последней стабильной версии Го, подходящего для этой системы
		if err := goin.FindURL(); err != nil {
			loggi.Fatal(err)
		}

		// Проверка на наличие обновлений
		if !goin.CheckUpdate() {
			loggi.Info("Обновление не требуется")
			os.Exit(0)
		}

		loggi.Info("Есть обновление.")

		// Загрузка последней стабильной версии Го, подходящего для этой системы
		if err := goin.Download(); err != nil {
			loggi.Fatal(err)
		}

		loggi.Info("Делаю резервную копию.")

		// Делает backup установленного Go
		if err := goin.MakeBackup(); err != nil {
			loggi.Fatal(err)
		}

		// Распаковка загруженного архива c Го в GOROOT
		if err := goin.Untar(goin.TmpFile, strings.Replace(goin.GOROOT, "go", "", 1)); err != nil {
			loggi.Fatal(err)
		}

		// Чистит за собой мусор
		if err := goin.Clean(); err != nil {
			loggi.Fatal(err)
		}
		loggi.Info("Обновлено.")
	}
}

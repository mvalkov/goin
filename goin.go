package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/meehalkoff/loggi"
)

// Goin - Основная структура приложения
type Goin struct {
	GOPATH             string
	GOROOT             string
	InstalledGoVersion string
	Downloader
	Archiver
}

// PreInstall совершает предустановочные проверки
func (g *Goin) PreInstall() error {

	// Проверка переменных окружения
	goroot := os.Getenv("GOROOT")
	gopath := os.Getenv("GOAPTH")

	if goroot != "" || gopath != "" {
		return fmt.Errorf("Переменные окружения Go уже установлены. Новая установка не возможна")
	}

	// Проверка на пустоту GOROOT директории
	_, err := os.Stat(g.GOROOT)
	if err == nil {
		return fmt.Errorf("%s не пуст", g.GOROOT)
	}

	return nil
}

// PreUpdate совершает предустановочные проверки
func (g *Goin) PreUpdate() error {
	// Проверка переменных окружения
	if os.Getenv("GOROOT") == "" {
		return fmt.Errorf("$GOROOT не найден")
	}

	g.GOROOT = os.Getenv("GOROOT")
	return nil
}

// CheckUpdate проверяет наличие обновлений
func (g *Goin) CheckUpdate() bool {
	path := filepath.Join(g.GOROOT, "VERSION")
	f, err := ioutil.ReadFile(path)
	if err != nil {
		loggi.Fatal(err)
	}

	g.InstalledGoVersion = strings.TrimSpace(string(f))

	if g.LatestGoVersion <= g.InstalledGoVersion {
		return false
	}
	return true
}

// MakeBackup делает резервную копию установленного Go
func (g *Goin) MakeBackup() error {
	return os.Rename(
		g.GOROOT,
		strings.Replace(g.GOROOT, "go", g.InstalledGoVersion, 1),
	)
}

// WriteGoEnv сохраняет переменные окружения
func (g *Goin) WriteGoEnv() error {
	// строка для записи
	s := fmt.Sprintf(
		"\nexport GOROOT=%s/go\nexport PATH=$PATH:$GOROOT/bin\nexport GOPATH=%s\nexport PATH=$PATH:$GOPATH/bin\n",
		g.GOROOT, g.GOPATH,
	)

	// выясняет какой shell используется в системе
	rc := ".bashrc"
	if strings.Contains(os.Getenv("SHELL"), "zsh") {
		rc = ".zshrc"
	}

	// открывает файл для записи
	f, err := os.OpenFile(
		filepath.Join(os.Getenv("HOME"), rc),
		os.O_APPEND|os.O_WRONLY, 0644,
	)
	defer f.Close()
	if err != nil {
		return err
	}

	// Записывает
	_, err = f.WriteString(s)
	if err != nil {
		return err
	}

	return nil
}

// Clean выполняет очистку после установки
func (g *Goin) Clean() error {
	return os.Remove(g.TmpFile)
}

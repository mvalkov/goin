package loggi

import (
	"fmt"
	"log"
	"os"
)

//Debug выводит отладочное сообщение
func Debug(v ...interface{}) {
	if os.Getenv("DEBUG") == "1" {
		fmt.Print("[DEBUG] ")
		log.Println(v...)
	}
}

//Info выводит информационное сообщение
func Info(v ...interface{}) {
	fmt.Print("[INFO]  ")
	log.Println(v...)
}

//Fatal выводит сообщение об ошибке
func Fatal(v ...interface{}) {
	fmt.Print("[FATAL] ")
	log.Fatalln(v...)
}

# goin

[![Go Report Card](https://goreportcard.com/badge/github.com/meehalkoff/goin)](https://goreportcard.com/report/github.com/meehalkoff/goin)
[![Build Status](https://travis-ci.org/meehalkoff/goin.svg?branch=master)](https://travis-ci.org/meehalkoff/goin)

## Установщик Go

```bash
 $ ./goin -h
Usage of ./goin:
  -gopath string
        -gopath /path/to/workplace (default "/home/$USER/Go/WorkPlace")
  -goroot string
        -goroot /path/to/go-sdk (default "/home/$USER/Go/SDK")
  -run string
        -run update or -run install (default "install")
```

---

## Использование

* Установка Go на чистую систему и настройка **$GOPATH** и **$GOROOT**
  > **$ ./goin**
  >
  > Авто установка. Скачает, распакует и настроит переменные окружения в соответствии с задаными параметрами по умолчанию.
  >
  > **$ ./goin -goroot /path/to/go/sdk -gopath /path/to/gopath**
  >
  > Аналогично вышеуказанному, только с параметрами.

  *В зависимости от используемого shell`a **goin** запишет переменные окружения в **~/.bashrc** для bash и **~/.zshrc** для zsh.*

---

* Обновление Go на новую версию, если установка осуществлялась с помощью **goin** или есть настроенный **$GOROOT**
  > **$ ./goin -run update**
  >
  > Автоматически проверит текущую установленную версию Go и доступную стабильную. Если обновление возможно - автоматически обновит.

package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"runtime"
	"strings"

	"github.com/meehalkoff/loggi"
)

// Downloader - структура с набором методов для осуществления поиска и загрузки архива с Go
type Downloader struct {
	RootURL         string
	LatestGoURL     string
	LatestGoVersion string
	TmpFile         string
}

//FindURL ищет ссылку на архив и проверяет версию Го
func (d *Downloader) FindURL() error {

	// поиск ссылки на архив с Го
	rg := regexp.MustCompile(fmt.Sprintf("(https.+?%s-%s.tar.gz)", runtime.GOOS, runtime.GOARCH))
	req, err := http.Get(d.RootURL)
	if err != nil {
		return err
	}
	defer req.Body.Close()

	a, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return err
	}
	d.LatestGoURL = rg.FindString(string(a))

	// поиск версии Го
	preprg := regexp.MustCompile(
		fmt.Sprintf("/go.+?%s", runtime.GOOS),
	)
	verrg := regexp.MustCompile(`\d+`)

	p := preprg.FindString(d.LatestGoURL)
	v := verrg.FindAllString(p, -1)
	d.LatestGoVersion = "go" + strings.Join(v, ".")

	loggi.Info(fmt.Sprintf("Доступная версия для загрузки: %s", d.LatestGoVersion))
	loggi.Debug(fmt.Sprintf("URL для загрузки: \t%s", d.LatestGoURL))
	return nil
}

// Download скачивает архив с Go
func (d *Downloader) Download() error {
	loggi.Info("Начинаю загрузку. Жди.")

	out, err := os.Create(d.TmpFile)
	if err != nil {
		return err
	}
	defer out.Close()

	resp, err := http.Get(d.LatestGoURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Ошибка. Ответ сервера: %s", resp.Status)
	}

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	loggi.Info("Загружено.")
	return nil
}
